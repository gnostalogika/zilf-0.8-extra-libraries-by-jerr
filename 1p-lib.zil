"First person ZIL/F library."

<GLOBAL NEW-PLAYER <>>

<SET REDEFINE T>
<ROUTINE PARSE-NOUN-PHRASE (WN NP "OPT" (SILENT? <>) "AUX" SPEC CNT W VAL MODE ADJ NOUN BUT SPEC-WN)
    <TRACE 3 "[PARSE-NOUN-PHRASE starting at word " N .WN "]" CR>
    <TRACE-IN>
    
    <SET SPEC <NP-YSPEC .NP 1>>
    <NP-NCNT .NP 0>
    <REPEAT ()
        <COND
            ;"exit loop if we reached the end of the command"
            (<G? .WN ,P-LEN>
             <TRACE 4 "[end of command]" CR>
             <RETURN>)
            ;"fail if we found an unrecognized word"
            (<NOT <OR <SET W <GETWORD? .WN>>
                      <AND <PARSE-NUMBER? .WN> <SET W ,W?\,NUMBER>>>>
             <TRACE 4 "[stop at unrecognized word: " WORD .WN "]" CR>
             <COND (<NOT .SILENT?>
                    <STORE-OOPS .WN>
                    <TELL "I don't know the word \"" WORD .WN "\"." CR>)>
             <TRACE-OUT>
             <RFALSE>)
            ;"exit loop if THEN or period"
            (<EQUAL? .W ,W?THEN ,W?\.>
             <TRACE 4 "[THEN at word " N .WN "]" CR>
             <RETURN>)
            ;"recognize BUT/EXCEPT"
            (<AND <NOT .BUT> <EQUAL? .W ,W?BUT ,W?EXCEPT>>
             <TRACE 4 "[BUT at word " N .WN "]" CR>
             <COND (<OR .ADJ .NOUN>
                    <OBJSPEC-ADJ .SPEC .ADJ>
                    <OBJSPEC-NOUN .SPEC .NOUN>
                    <SET ADJ <SET NOUN <>>>
                    <SET CNT <+ .CNT 1>>)>
             <TRACE 4 "[saving " N .CNT " YSPEC(s)]" CR>
             <NP-YCNT .NP .CNT>
             <SET BUT T>
             <SET SPEC <NP-NSPEC .NP 1>>
             <SET CNT 0>)
            ;"recognize ALL/ANY/ONE"
            (<EQUAL? .W ,W?ALL ,W?EVERY ,W?EVERYTHING ,W?ANY ,W?ONE>
             <COND (<OR .MODE .ADJ .NOUN>
                    <TRACE 4 "[too late for mode change at word " N .WN "]" CR>
                    <COND (<NOT .SILENT?>
                           <TELL "I can't use \"" B .W "\" there." CR>)>
                    <TRACE-OUT>
                    <RFALSE>)>
             <SET MODE
                  <COND (<EQUAL? .W ,W?ALL ,W?EVERY ,W?EVERYTHING> ,MCM-ALL)
                        (ELSE ,MCM-ANY)>>
             <TRACE 4 "[mode change at word " N .WN ", now mode=" N .MODE "]" CR>
             <SET SPEC-WN .WN>)
            ;"match adjectives, keeping only the first"
            (<VERSION?
                (ZIP <SET VAL <WORD? .W ADJECTIVE>>)
                (ELSE <CHKWORD? <SET VAL .W> ,PS?ADJECTIVE>)>
             <TRACE 4 "[adjective '" B .W "' at word " N .WN "]" CR>
             ;"if we already have a noun, this must start a new noun phrase"
             <COND (.NOUN
                    <TRACE 4 "[terminating]" CR>
                    <RETURN>)>
             <SET SPEC-WN .WN>
             <COND
                 ;"if W can also be a noun, treat it as such if
                   it isn't followed by an adj or noun"
                 (<AND <CHKWORD? .W ,PS?OBJECT>         ;"word can be a noun"
                       <OR ;"word is at end of line"
                           <==? .WN ,P-LEN>
                           ;"next word is not adj/noun"
                           <BIND ((NW <GETWORD? <+ .WN 1>>))
                               <NOT <OR <CHKWORD? .NW ,PS?ADJECTIVE>
                                        <CHKWORD? .NW ,PS?OBJECT>>>>>>
                  <TRACE 4 "[treating it as a noun]" CR>
                  <SET NOUN .W>)
                 (<==? .CNT ,P-MAX-OBJSPECS>
                  <TRACE 4 "[already have " N .CNT " specs]" CR>
                  <COND (<NOT .SILENT?>
                         <TELL "That phrase mentions too many objects." CR>)>
                  <TRACE-OUT>
                  <RFALSE>)
                 (<NOT .ADJ>
                  <SET ADJ .VAL>)
                 (ELSE
                  <TRACE 4 "[ignoring it]" CR>)>)
            ;"match nouns, exiting the loop if we already found one"
            (<CHKWORD? .W ,PS?OBJECT>
             <TRACE 4 "[noun '" B .W "' at word " N .WN "]" CR>
             <COND (.NOUN
                    <TRACE 4 "[terminating]" CR>
                    <RETURN>)
                   (<==? .CNT ,P-MAX-OBJSPECS>
                    <TRACE 4 "[already have " N .CNT " specs]" CR>
                    <COND (<NOT .SILENT?>
                           <TELL "That phrase mentions too many objects." CR>)>
                    <TRACE-OUT>
                    <RFALSE>)
                   (ELSE
                    <SET NOUN .W>
                    <SET SPEC-WN .WN>)>)
            ;"recognize AND/comma"
            (<EQUAL? .W ,W?AND ,W?COMMA>
             <TRACE 4 "[AND at word " N .WN "]" CR>
             <COND (<OR .ADJ .NOUN>
                    <OBJSPEC-ADJ .SPEC .ADJ>
                    <OBJSPEC-NOUN .SPEC .NOUN>
                    <SET ADJ <SET NOUN <>>>
                    <SET SPEC <REST .SPEC ,P-OBJSPEC-SIZE>>
                    <SET CNT <+ .CNT 1>>
                    <TRACE 4 "[now have " N .CNT " spec(s)]" CR>)>)
            ;"recognize OF"
            (<AND <EQUAL? .W ,W?OF>
                  <L? .WN ,P-LEN>
                  <STARTS-NOUN-PHRASE? <GETWORD? <+ .WN 1>>>>
             ;"This is a hack to deal with object names consisting of multiple NPs
               joined by OF. When we see OF before a word that could start a new
               noun phrase, we forget the current noun, so SMALL PIECE OF TASTY PIE
               parses as SMALL TASTY PIE (which in turn parses as SMALL PIE)."
             <TRACE 4 "[OF at word " N .WN ", clearing noun]" CR> 
             <SET NOUN <>>)
            ;"skip buzzwords"
            (<CHKWORD? .W ,PS?BUZZ-WORD>
             <TRACE 4 "[skip buzzword at word " N .WN "]" CR>
             <SET SPEC-WN .WN>)
            ;"exit loop if we found any other word type"
            (ELSE
             <TRACE 4 "[bail over type at word " N .WN "]" CR>
             <RETURN>)>
        <SET WN <+ .WN 1>>>
    ;"store final adj/noun pair"
    <COND (<OR .ADJ .NOUN>
           <OBJSPEC-ADJ .SPEC .ADJ>
           <OBJSPEC-NOUN .SPEC .NOUN>
           <SET CNT <+ .CNT 1>>
           <TRACE 4 "[finally have " N .CNT " spec(s)]" CR>)>
    ;"store phrase count and mode"
    <COND (.BUT <NP-NCNT .NP .CNT>) (ELSE <NP-YCNT .NP .CNT>)>
    <NP-MODE .NP .MODE>
    <TRACE 2 "[noun phrase parsed: " NOUN-PHRASE .NP "]" CR>
    <TRACE-OUT>
    <+ .SPEC-WN 1>>

<SET REDEFINE T>
<ROUTINE MANY-CHECK (OBJ OPTS INDIRECT?)
    <COND (<AND <=? .OBJ ,MANY-OBJECTS>
                <NOT <BTST .OPTS ,SF-MANY>>>
           <COND (<VERB? TELL>
                  <TELL "I can only address one person at a time.">)
                 (ELSE
                  <TELL "I can't use multiple ">
                  <COND (.INDIRECT? <TELL "in">)>
                  <TELL "direct objects with \"">
                  <PRINT-VERB>
                  <TELL "\".">)>
           <CRLF>
           <SETG P-CONT 0>
           <RFALSE>)>
    <RTRUE>>

<SET REDEFINE T>
<ROUTINE HAVE-TAKE-CHECK-TBL (TBL OPTS "AUX" MAX DO-TAKE? O N ORM)
    <SET MAX <GETB .TBL 0>>
    ;"Attempt implicit take if WINNER isn't directly holding the objects"
    <COND (<BTST .OPTS ,SF-TAKE>
           <DO (I 1 .MAX)
               <COND (<SHOULD-IMPLICIT-TAKE? <GET/B .TBL .I>>
                      <TELL "[taking ">
                      <SET N <LIST-OBJECTS .TBL ,SHOULD-IMPLICIT-TAKE? %<+ ,L-PRSTABLE ,L-THE>>>
                      <TELL "]" CR>
                      <REPEAT ()
                          <COND (<SHOULD-IMPLICIT-TAKE? <SET O <GET/B .TBL .I>>>
                                 <COND (<NOT <TRY-TAKE .O T>>
                                        <COND (<G? .N 1>
                                               <SET ORM ,REPORT-MODE>
                                               <SETG REPORT-MODE ,SHORT-REPORT>
                                               <TELL D .O ": ">
                                               <TRY-TAKE .O>
                                               <SETG REPORT-MODE .ORM>)
                                              (ELSE
                                               <TRY-TAKE .O>)>
                                        <RFALSE>)>)>
                          <COND (<IGRTR? I .MAX> <RETURN>)>>
                      <RETURN>)>>)>
    ;"WINNER must (indirectly) hold the objects if SF-HAVE is set"
    <COND (<BTST .OPTS ,SF-HAVE>
           <DO (I 1 .MAX)
               <COND (<FAILS-HAVE-CHECK? <GET/B .TBL .I>>
                      <TELL "I am not holding ">
                      <LIST-OBJECTS .TBL ,FAILS-HAVE-CHECK? %<+ ,L-PRSTABLE ,L-THE ,L-OR>>
                      <TELL "." CR>
                      <SETG P-CONT 0>
                      <RFALSE>)>>)>
    <RTRUE>>


<SET REDEFINE T>
<ROUTINE MATCH-NOUN-PHRASE (NP OUT BITS "AUX" F NY NN SPEC MODE NOUT OBITS ONOUT BEST Q)
    <SET NY <NP-YCNT .NP>>
    <SET NN <NP-NCNT .NP>>
    <SET MODE <NP-MODE .NP>>
    <SET OBITS .BITS>
    <COND (<0? .MODE>
           <SET .BITS <ORB .BITS %<ORB ,SF-HELD ,SF-CARRIED ,SF-ON-GROUND ,SF-IN-ROOM>>>)>
    <TRACE 3 "[MATCH-NOUN-PHRASE: NY=" N .NY " NN=" N .NN " MODE=" N .MODE
             " BITS=" N .BITS " OBITS=" N .OBITS "]" CR>
    <TRACE-IN>
    <PROG BITS-SET ()
        ;"Look for matching objects"
        <SET NOUT 0>
        <COND (<0? .NY>
               ;"ALL with no YSPECs matches all objects, or if the action is TAKE/DROP,
                 all objects with TAKEBIT/TRYTAKEBIT, skipping generic/global objects."
               <TRACE 4 "[applying ALL rules]" CR>
               <MAP-SCOPE (I [BITS .BITS])
                   <COND (<SCOPE-STAGE? GENERIC GLOBALS>)
                         (<NOT <ALL-INCLUDES? .I>>)
                         (<AND .NN <NP-EXCLUDES? .NP .I>>)
                         (<G=? .NOUT ,P-MAX-OBJECTS>
                          <TELL "[too many objects!]" CR>
                          <TRACE-OUT>
                          <RETURN>)
                         (ELSE
                          <SET NOUT <+ .NOUT 1>>
                          <PUT/B .OUT .NOUT .I>)>>)
              (ELSE
               ;"Go through all YSPECs and find matching objects for each one.
                 Give an error if any YSPEC has no matches, but it's OK if all
                 the matches for some YSPEC are excluded by NSPECs. Keep track of
                 the match quality and only select the best matches."
               <DO (J 1 .NY)
                   <SET SPEC <NP-YSPEC .NP .J>>
                   <TRACE 4 "[SPEC=" OBJSPEC .SPEC "]" CR>
                   <SET F <>>
                   <SET ONOUT .NOUT>
                   <SET BEST 1>
                   <MAP-SCOPE (I [BITS .BITS])
                       <TRACE 5 "[considering " T .I "]" CR>
                       <COND (<AND <NOT <FSET? .I ,INVISIBLE>>
                                   <SET Q <REFERS? .SPEC .I>>
                                   <G=? .Q .BEST>>
                              <TRACE 4 "[matches " T .I "(" N .I "), Q=" N .Q "]" CR>
                              <SET F T>
                              ;"Erase previous matches if this is better"
                              <COND (<G? .Q .BEST>
                                     <TRACE 4 "[clearing match list]" CR>
                                     <SET NOUT .ONOUT>
                                     <SET .BEST .Q>)>
                              <COND (<AND .NN <NP-EXCLUDES? .NP .I>>
                                     <TRACE 4 "[excluded]" CR>)
                                    (<G=? .NOUT ,P-MAX-OBJECTS>
                                     <TELL "[too many objects!]" CR>
                                     <TRACE-OUT>
                                     <RETURN>)
                                    (ELSE
                                     <TRACE 4 "[accepted]" CR>
                                     <SET NOUT <+ .NOUT 1>>
                                     <PUT/B .OUT .NOUT .I>)>)>>
                   ;"Look for a pseudo-object if we didn't find a real one."
                   <COND (<AND <NOT .F>
                               <BTST .BITS ,SF-ON-GROUND>
                               <SET Q <GETP ,HERE ,P?THINGS>>>
                          <TRACE 4 "[looking for pseudo]" CR>
                          <SET F <MATCH-PSEUDO .SPEC .Q>>
                          <COND (.F
                                 <COND (<AND .NN <NP-EXCLUDES-PSEUDO? .NP .F>>)
                                       (<G=? .NOUT ,P-MAX-OBJECTS>
                                        <TELL "[too many objects!]" CR>
                                        <TRACE-OUT>
                                        <RETURN>)
                                       (ELSE
                                        <SET NOUT <+ .NOUT 1>>
                                        <PUT/B .OUT .NOUT <MAKE-PSEUDO .F>>)>)>)>
                   <COND (<NOT .F>
                          ;"Try expanding the search if we can."
                          <COND (<N=? .BITS -1>
                                 <TRACE 4 "[expanding to ludicrous scope]" CR>
                                 <SET BITS -1>
                                 <SET OBITS -1>    ;"Avoid bouncing between <1 and >1 matches"
                                 <AGAIN .BITS-SET>)>
                          <COND (<=? ,MAP-SCOPE-STATUS ,MS-NO-LIGHT>
                                 <TELL "It's too dark to see anything here." CR>)
                                (ELSE
                                 <TELL "I don't see that here." CR>)>
                          <TRACE-OUT>
                          <RFALSE>)
                         (<G=? .NOUT ,P-MAX-OBJECTS>
                          <TRACE-OUT>
                          <RETURN>)>>)>
        ;"Check the number of objects"
        <PUTB .OUT 0 .NOUT>
        <COND (<0? .NOUT>
               ;"This means ALL matched nothing, or BUT excluded everything.
                 Try expanding the search if we can."
               <SET F <ORB .BITS %<ORB ,SF-HELD ,SF-CARRIED ,SF-ON-GROUND ,SF-IN-ROOM>>>
               <COND (<=? .BITS .F>
                      <TELL "There are none at all available!" CR>
                      <TRACE-OUT>
                      <RFALSE>)>
               <TRACE 4 "[expanding to reasonable scope]" CR>
               <SET BITS .F>
               <SET OBITS .F>    ;"Avoid bouncing between <1 and >1 matches"
               <AGAIN .BITS-SET>)
              (<1? .NOUT>
               <TRACE-OUT>
               <RETURN <GET/B .OUT 1>>)
              (<OR <=? .MODE ,MCM-ALL> <G? .NY 1>>
               <TRACE-OUT>
               <RETURN ,MANY-OBJECTS>)
              (<=? .MODE ,MCM-ANY>
               ;"Pick a random object"
               <PUT/B .OUT 1 <SET F <GET/B .OUT <RANDOM .NOUT>>>>
               <PUTB .OUT 0 1>
               <TELL "[" T .F "]" CR>
               <TRACE-OUT>
               <RETURN .F>)
              (ELSE
               ;"TODO: Do this check when we're matching YSPECs, so each YSPEC can be
                 disambiguated individually."
               ;"Try narrowing the search if we can."
               <COND (<N=? .BITS .OBITS>
                      <TRACE 4 "[narrowing scope to BITS=" N .OBITS "]" CR>
                      <SET BITS .OBITS>
                      <AGAIN .BITS-SET>)>
               <COND (<SET F <APPLY-GENERIC-FCN .OUT>>
                      <TRACE 4 "[GENERIC chose " T .F "]" CR>
                      <PUT/B .OUT 1 .F>
                      <PUTB .OUT 0 1>
                      <TRACE-OUT>
                      <RETURN .F>)>
               <WHICH-DO-YOU-MEAN .OUT>
               <COND (<=? .NP ,P-NP-DOBJ> <ORPHAN T AMBIGUOUS PRSO>)
                     (ELSE <ORPHAN T AMBIGUOUS PRSI>)>
               <TRACE-OUT>
               <RFALSE>)>>>

<SET REDEFINE T>
<OBJECT PLAYER>