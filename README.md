Here are extra ZILF libraries...  Delete starting at the hyphen on all VERBS and stopping at .ZIL, and insert said VERBS.ZIL with respective *P-LIB.ZIL files.    
This will give your game first or third person definition to your ZIL/F storyfiles. 

Insert NEW-PLAYER.ZIL before the parser library to get this to work.  Make sure to SETG WINNER ,NEW-PLAYER and also SETG on NEW-PLAYER to an NPC object as your new player object in your GO 
routine.  The rest should be easy for you to figure out!  Good luck and implement big!

-Jerr